package com.example.bottonnavigation2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import com.example.bottonnavigation2.databinding.ActivityMainBinding;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MainActivity extends AppCompatActivity {

    ActivityMainBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        //String searchWord = "сы";

        setContentView(binding.getRoot());
        replaceFragment(new HomeFragment());



        binding.bottomNavigationView2.setOnItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.home:
                    replaceFragment(new HomeFragment());
                    break;
                case R.id.profile:
                    replaceFragment(new ProfileFragment());
                    break;
                case R.id.settings:
                    replaceFragment(new SettingsFragment());
                    break;
            }
            return true;
        });

    }




//    private ArrayList<Food> JsonToArrayList() throws IOException {
//        ObjectMapper mapper = new ObjectMapper();
//
//        ArrayList<Food> arrayList = mapper.readValue(new File("assets/json.json"), new TypeReference<ArrayList<Food>>(
//
//        ) {});
//
//        System.out.println(arrayList);
//        return arrayList;
//    }

//    private ArrayList<String> getArrayListName(ArrayList<Food> arrayList) {
//        ArrayList<String> nameList = new ArrayList<>();
//        for (int i = 0; i < arrayList.size(); ++i) {
//            nameList.add(arrayList.get(i).getName());
//        }
//        return nameList;
//    }

    private void replaceFragment(Fragment fragment) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout, fragment);
        fragmentTransaction.commit();
  }
}